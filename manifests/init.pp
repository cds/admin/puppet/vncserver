class vncserver (
    String $package_name,
    String $service_user,
) {
    package {"${package_name}":
        ensure => 'installed',
    }

    exec {'systemd-reload-vncserver':
        command => '/bin/systemctl daemon-reload',
	refreshonly => true,
    }

    file {'/etc/systemd/system/vncserver@.service.d':
        ensure => 'directory',
	require => Package["${package_name}"],
    }

    file {'/etc/systemd/system/vncserver@.service.d/cmdline.conf':
        ensure => 'file',
	owner => 'root',
	group => 'root',
	mode => '0644',
	content => epp('vncserver/cmdline.conf.epp', {
	    'user' => $service_user,
	}),
	require => Package["${package_name}"],
	notify => Exec['systemd-reload-vncserver'],
    }
}
