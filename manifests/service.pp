define vncserver::service (
    Integer $display,
    Integer $width,
    Integer $height,
    Boolean $service_enable = true,
    Enum['running', 'stopped'] $service_ensure = 'running',
) {
    include ::vncserver

    file {"/etc/sysconfig/vncserver-:${display}":
	ensure => 'file',
        owner => 'root',
	group => 'root',
	mode => '0644',
	content => epp('vncserver/sysconfig.epp', {
	    width => $width,
	    height => $height,
	}),
	notify => Service["vncserver@:${display}"],
    }

    Exec['systemd-reload-vncserver'] ->
    service {"vncserver@:${display}":
        enable => $service_enable,
        ensure => $service_ensure,
    }
}
